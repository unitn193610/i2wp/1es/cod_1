package cod_1;

import java.io.*;
import java.net.*;

//Per comodità importiamo direttamente tutto ciò che ci serve,
//ovvero java.io.* e java.net.*

public class Client
{
    private Socket mySocket = null;

    private BufferedReader in;
    private PrintWriter out;
    private BufferedReader tastiera;

    public Socket Connetti(int PORT)
    {
        /*
        Ricordiamo che il TCP è un porotocollo orientato alla connesione. Ciò significa che, prima di iniziare
        a scambiarsi dati, client e server devono effettuare un'operazione di handshake e stabilire una connessione TCP.
         */
        try {
                 System.out.println("[0] - Provo a connettermi al server..." );

            /*
            Posso inserire il mio indirizzo ip o il nome del mio mac.
            Mi viene dunque in aiuto il metodo getLocalHost() della classe InetAddress
            che anticipando quanto vedremo in cod_2, è una classe che svolge, tra le altre molte sue funzioni,
            un servizio di traduzione. In altre parole, opera un servizio del DNS.
             */
            mySocket = new Socket(InetAddress.getLocalHost(), PORT);

        }
        catch(UnknownHostException e){
            System.err.println("Host sconosciuto!");
            e.printStackTrace(System.err);
        }
        catch(Exception e){
            System.err.print("Impossibile stabilire la connessione");
            e.printStackTrace(System.err);
            System.exit(1);
        }
        finally {
            try{
                System.out.println("[1] - Connesso!");
                //creo canali di comunicazione
                in = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(mySocket.getOutputStream())), true);
            }
            catch(IOException e){
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }

        return mySocket;
    }

    public void Comunica()
    {
        /*
        Una volta stabilita la connessione TCP, quando un lato vuole inviare dati all'altro,
        deve solo mettere i dati nella connessione TCP attraverso la sua socket.
         */
        try {
            tastiera = new BufferedReader(new InputStreamReader(System.in));

                    System.out.print("[2] - Messaggio da inviare al server: ");

            String msg_client = tastiera.readLine();

                    System.out.println("[3] - Invio: " + msg_client);

            out.println(msg_client);

                    System.out.println("[4] - In attesa di una risposta...");

            //la risposta del server entrerà nel flusso di entrata del client.
            String res_server = in.readLine();

                    System.out.println(("[5] - Risposta del server: " + res_server));

        }catch(IOException e){
            System.err.println("Errore di I/O: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        finally {
            try{
                in.close();
                out.close();
                mySocket.close();
            }
            catch(Exception e){
                System.err.println("Errore di chiusura: " + e.getMessage());
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }
    }

    public static void main(String[] args)
    {
        int PORT = 3000; //qui metto la porta del Server

        Client c = new Client();
        c.Connetti(PORT);
        /*
        Se faccio partire solo Client.java otterrò un messaggio d'errore, ovvero
        l'impossibilità di connettermi dal momento che il Server è "spento".
        Dovrò quindi far partire, in parole informali, entrambi i codici.

        Il server dovrà essere in ascolto, e quindi "accesso" --> primo da far partire
        Se infatti farò partire solo Server.java esso rimmarrà in esecuzione (in "run")
        finchè, in questo caso, UN client non si connetterà.

        Il client potrà connettersi al server in qualsiasi momento --> far partire per secondo
        */
        c.Comunica();
    }
}