# Socket

La maggior parte delle applicazioni consiste di coppie di processi comunicanti che si scambiano messaggi. Ogni messaggio inviato da un processo a un altro deve passare attraverso la rete sottostante. Un processo invia messaggi nella rete e riceve messaggi dalla rete attraverso un'interfaccia software detta **socket**.

Usiamo un'analogia che ci aiuterà a comprendere i processi e le socket:

*Un processo è assimilabile a una casa e le socket sono il corrispettivo delle porte.* 

Un processo che vuole inviare un messaggio a un altro processo o a un altro host, fa uscire il messaggio dalla propria porta (socket). Il processo presuppone l'esistenza di un'infrastruttura esterna che trasporterà il messaggio attraverso la rete fino alla porta del processo di destinazione. Quando il messaggio giunge al destinatario, attraversa la porta (socket) del processo ricevente che infine opera sul messaggio.

In Internet, gli host vengono identificati, ricordiamo, attraverso i loro indirizzi IP (numero di 32 bit univoco). Oltre a conoscere l'indirizzo dell'hst cui è destinato il messaggio, il mittente deve anche identificare il processo destinatario, più specificatamente la socket che deve ricevere il dato. Questa informazione è necessaria in quanto, in generale, sull'host potrebbero essere in esecuzione molte applicazioni di rete. Un numero di porta di destinazione assolve questo compito. 

# Java Socket
Il Java Networking API (java.net) fornisce le classe e interfacce per assolvere quanto descritto nel paragrafo precedente, ovvero:
- Indirizzamento:
    - InetAddress
- Creazione di connessioni TCP (esercizi su cui si baserà l'esercitazione e l'intera repository):
    - ServerSocket (classe per lato Server);
    - Socket (classe per lato Client)
- Creazione di connessioni UDP (ricordiamo, non affidabili ma veloci):
    - DatagramPacket;
    - DatagramSocket;
- Localizzare risorse di rete
    - URL, URLConnection, HttpURLConnection
- Sicurezza (autenticazione, permessi)

# La classe InterAddress
Il metodo getByName() può impiegare il DNS e recuperare l'indirizzo IP associato all'hostname fornito come argomento
- Si può specificare nel suo argomento sia un indirizzo IP nella forma decimale che il nome di un host;
- In entrambi i casi, l'oggetto InetAddress restituito conterrà l'indirizzo IP a 32 bit.

Nel caso che ad un hostname siano associati più indirizzi IP, li si può ottenere chiamando la getAllByName();

La getLocalHost restituisce l'indirizzo IP dell'host su cui viene eseguita;

getHostName e getHostAddress restituiscono rispetivamente il nome e l'indirizzo IP che rappresentano.

# Classi per il protocollo TCP
Java fornisce due diverse classi per la comunicazione con il protocollo TCP che rispecchiano la struttura client/server:
- creazione socket per il sever: classe ServerSocket;
- creazione socket per il client: classe Socket;

La differenziazione del socket Client e Server è dovuto alle diverse operazioni che vengono svolte al momento di stabilire una connessione.
- Un server ottiene l'oggetto socket da una chiamata al metodo accept.
- il client deve provvedere a creare un'istanza del socket.

# Classe ServerSocket
Implementa un server socket: viene utilizzata da server che accettano connessioni dai client.
Costruttori:
- ServerSocket();
- ServerSocket(int port);
- ServerSocket(int port, int backlog);
- ServerSocket(int port, int backlog,InetAddress bindAdd)

Il parametro port specifica la porta su cui rimanere in attesa, il parametro backlog il numero massimo di richieste di connessione (default 50) mentre il parametro bindAdd viene utilizzato dai server multihomed (con più interfacce di rete) per specificare un determinato indirizzo.

Non è necessario specificare la famiglia dei protocolli: si opera sempre con IP.
L'impiego di questa classe implica l'uso del protocollo TCP.
Un eventuale errore genera una eccezione IOException sollevata dai costruttori.

IMPORTANTE

Il metodo più importante è **accept**, che blocca il server in attesa di una connessione. È questo il metodo che restituisce un oggetto di tipo socket (con le stesse proprietà di quello originale) completamente istanziato nei parametri (dati locali e remoti) che viene poi utilizzato per gestire la comunicazione con il client.
*Socket newconnection = myServerSocket.accept()*;
Solo dopo che il metodo accept() ritorna un socket valido è possibile eseguire operazioni di I/O su quel socket.

Sono disponibili anche altri metodi che permettono di gestire altri aspetti legati ai socket, come ad esempio timeout o buffer:
- setSoTimeout(int timeout): fissa il tempo massimo di attesa per una accept;
- setReceiveBufferSize(int size): permette di settare la dimensione del buffer di ricezione associato al socket.

# Classe Socket
Rappresenta una socket client ed è quindi un estremo della comunicazione fra due macchine. 
Costruttori:
- Public Socket(String host, int Port);
- Pubic Socket(InetAddress, int Port);

Questi costruttori specificano nel primo argomento l’host remoto e nel secondo la porta del server con cui effettuare la connessione. Altri costruttori permettono di creare il socket associandolo ad una determinata porta e indirizzo locale-

Anche qui non è necessario specificare la famiglia dei protocolli: si opera sempre con IP.
L'impiego di questa classe implica l'uso del protocollo TCP.
Può generare le eccezioni:
- IOException: errore creato alla creazione del socket;
- UnknownHostException: l'host non è stato trovato.


Questa classe fornisce alcuni metodi per poter estrarre e manipolare informazioni sul socket creato:
- getLocalAddress, getInetAddress, getPort, getLocalPort: forniscono gli indirizzi e le porte di accesso della connesione;
- setTcpNoDelay: forza la spedizione dei dati senza che il pacchetto TCP sia copmleto;
- sendUrgentData: spedisce un byte di dati urgenti sul socket scavalcando altre operazioni di write in sospeso;
- setSoTimeout: specifica il tempo di attesa in lettura sul socket;
- setSendBufferSize, setReceiveBufferSize: specificano le dimensioni dei buffer di lettura e scrittura.

# Metodi per Input/Output dei dati:
- InputStream getInputStream(): restituisce uno stream in lettura per il socket;
- OutputStream getOutputStream(): restituisce uno stream in scrittura per il socket.


# Gestione degli errori
È importante che i socket siano prioramente chiusi al termine da un applicazione mediante il metodo **close()** delle classi (sia Socket che ServerSocket) poichè questi sono una risorsa di rete del sistema.

La chiusura dei socket deve essere eseguita indipendentemente dal flusso di esecuzione del programma, utilizzando per questo le sezioni try e finally.


