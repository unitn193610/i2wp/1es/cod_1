package cod_1;

import java.io.*;
import java.net.InetAddress; //.....Classe per la gestione degli indirizzi
import java.net.ServerSocket; //....Classe per la creazione di socket lato server
import java.net.Socket; //..........Classe per la creazione di socket lato client

public class Server
{
//*******  ATTRIBUTI  ******
    private ServerSocket server;
    private Socket socketClient;

    private BufferedReader in; //  Queste due classi mi permettono di attivare uno stream di comunicazione
    private PrintWriter out;// con il cliente
//*************************

//****** COSTRUTTORE ******
    private Server(){
        server = null; //Finchè non viene stabilità nessuna connessione, il collegamento non c'è.
        socketClient = null;  //Se si presentasse un errore, otterrò null (piuttosto che parametri random auto-generati).
        in = null;
        out = null;
    }
//*************************

    public Socket Connetti(int PORT)
    {
        try {
                    System.out.println("[0] - Inizializzo il server...");

            server = new ServerSocket(PORT);
            //Ci connettiamo alla porta prestabilita tramite il costruttore ServerSocket(int port);

                    System.out.println("[1] - Server pronto sulla porta: " + PORT);

        }catch(Exception e) {
            System.err.println("Errore durante la connessione alla porta: " + PORT);
            e.printStackTrace(System.err);
            System.err.println(e.getMessage());
            System.exit(1);
        }

        try{
            socketClient = server.accept();
            //Accetto le chiamate Client (di tipo socket) sull'oggetto server (di tipo socket).

                    System.out.println("[2] - Connessione stabilita con UN client!");

            /*
               La classe InetAddress ed i metodi getInetAddress e getLocalPort
               servono per identificare, rispettivamente, il nome del server e la porta impiegata.
               Il metodo getInetAddress della classe ServerSocket permette di ottenere un dato che
               deve essere analizzato dalla classe InetAddress per poterne ricavare un valore stringa
               comprensibile.
             */

            //Qui andiamo a stampare in output le informazioni relative al server in ascolto e al client in accesso.
            InetAddress indirizzoServer = server.getInetAddress();
            String indirizzo_leggibile_server = indirizzoServer.getHostAddress();
            int porta = server.getLocalPort();

            System.out.println("\nServer info: \n"+
                    "Host Address: " + indirizzo_leggibile_server +"\n"+
                    "Host Name: " + indirizzoServer.getHostName() + "\n" +
                    "Local Port: " + porta + "\n" +
                    "Local Address: " + server.getLocalSocketAddress()
            );

            InetAddress infoClient = socketClient.getInetAddress();

            System.out.println("\nClient info: \n"+
                "Host Address: " + infoClient.getHostAddress() +"\n"+
                "Host Name: " + infoClient.getHostName() + "\n" +
                "Local Port: " + socketClient.getLocalPort() + "\n" +
                "Local Address: " + socketClient.getLocalAddress()
            );

            in = new BufferedReader(new InputStreamReader(socketClient.getInputStream())); //leggo richieste del client
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream())), true); //rispondo al client

        }catch (IOException e){
            System.err.println("Errore nei canali di comunicazione di I/O: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
        finally{
            /*
            Dopo che la connessione tra client-server è avvenuta, la chiudiamo in modo
            da avere UN unica connessione punto a punto. Evitiamo dunque altre connessioni in ingresso.
            In altre parole, chiudiamo i canali di comunicazione.
             */
            try{
                server.close();
            }
            catch(Exception e){
                System.err.println("Errore duarnte la chiusura del server: " + e.getMessage());
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }

        return socketClient;
        //NOTA: se ci sonno errori, avviene la return null.
    }

    public void Comunica()
    {
        try {
                    System.out.println("\n[3] - Aspetto un messaggio dal Client...");

            String msg_client = in.readLine(); // OUT_CLIENT --> IN_SERVER (in.readLine())

                    System.out.println("[4] - Messaggio ricevuto: " + msg_client);

            String res_server; //risposta che il server dà al client
            /*
            Facciamo finta che il client ha richiesto un servizio che
            consiste nel trasformare un messagio di testo da minuscolo
            a maiuscolo.
             */
            if (msg_client.contentEquals("omae wa mou shindeiru") || msg_client.contentEquals("OMAE WA MOU SHINDEIRU"))
                res_server = "NANI!?!?!?";
            else
                res_server = msg_client.toUpperCase();

                    System.out.println("[5] - Rispondo con: " + res_server);

            out.println(res_server);
            // La risposta del server viaggerà tramite lo stream di comunicazione out del server

        } catch (IOException e) {
            System.err.println("Errore durante la lettura: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        finally {
            try {
                socketClient.close(); // <----- IMPORTANTE
                /*
                RICORDA DI CHIUDERE LA CONNESSIONE COL CLIENT
                UNA VOLTA CHE IL SERVIZIO E' STATO CONCLUSO!
                RICORDARSI ANCHE DI CHIUEDERE I CANALI DI COMUNICAZIONE!
                 */
                in.close();
                out.close();
            } catch (Exception ex) {
                System.err.println("Errore durante la chiusura: " + ex.getMessage());
                ex.printStackTrace(System.err);
                System.exit(1);
            }
        }
    }

    public static void main(String[] args)
    {
        int PORT = 3000;

        Server s = new Server();
        s.Connetti(PORT);
        s.Comunica();
    }
}